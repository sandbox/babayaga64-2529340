name = Siren
description = A 2 column layout with a prominent header region.
preview = preview.png
template = siren-layout

; Regions
regions[branding]       = Branding
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[preface]        = Preface
regions[content]        = Content
regions[sidebar]        = Sidebar
regions[epilogue]       = Epilogue
regions[errata]         = Errata
regions[footer]         = Footer

; Stylesheets
stylesheets[all][] = css/layouts/siren/siren.layout.css
stylesheets[all][] = css/layouts/siren/siren.layout.no-query.css
