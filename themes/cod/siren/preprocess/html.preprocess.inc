<?php

/**
* Implements hook_preprocess_html().
*/
  function siren_preprocess_html(&$variables) {

    if(arg(3) == 'proposed') {
      $track = arg(4);
      if(isset($track)) {
        $argument = check_plain(arg(4));
        $vars['attributes_array']['class'][] = $argument;
      }
    }

    $page = page_manager_get_current_page();
    if($page) {
      $layout_name = $page['handler']->conf['display']->layout;
      $variables['classes_array'][] = 'panels-layout';
      if($layout_name) {
        $variables['classes_array'][] = 'panels-layout-' . $layout_name;
      }
    }
    else {
      $variables['classes_array'][] = 'no-panels-layout';
    }
  }